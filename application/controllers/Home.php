<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data');
		$this->load->library(array('form_validation', 'session'));
	}

	public function index()
	{
		$data['data'] = $this->Data->tampil_data();
		$this->load->view('header');
		$this->load->view('main_dashboard', $data);
		$this->load->view('footer');
	}

	public function main_dashboard()
	{
		$data['data'] = $this->Data->tampil_data();
		$this->load->view('header');
		$this->load->view('main_dashboard', $data);
		$this->load->view('footer');
	}

	public function login()
	{
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function history()
	{
		$this->load->view('header');
		$this->load->view('history');
		$this->load->view('footer');
	}

	public function buttonn()
	{
		$this->load->view('header');
		$this->load->view('buttonn');
		$this->load->view('footer');
	}

	public function patching_dashboard()
	{
		$this->load->view('header');
		$this->load->view('patching_dashboard');
		$this->load->view('footer');
	}

	public function patching_rutin()
	{
		$this->load->view('header');
		$this->load->view('patching_rutin');
		$this->load->view('footer');
	}

	public function patching_nonrutin()
	{
		$this->load->view('header');
		$this->load->view('patching_nonrutin');
		$this->load->view('footer');
	}

	public function log_document()
	{
		$this->load->view('header');
		$this->load->view('log_document');
		$this->load->view('footer');
	}

	public function new_script()
	{
		$data['datas'] = $this->Data->tampil_datatable();
		$this->load->view('header', $data);
		$this->load->view('new_script', $data);
		$this->load->view('footer', $data);
	}

	function hapus_script()
	{
		$id_kode = $this->input->post('_id_kode');
		$this->id_kode->hapus_script($id_kode);
		redirect('id_kode');
	}

	public function insert_script()
	{
		$keterangan = $this->input->post('keterangan');
		$id_kode = $this->input->post('id_kode');
		$nama_dokumen = $this->input->post('nama_dokumen');
		$id_kategori = $this->input->post('id_kategori');
		$id_status = $this->input->post('id_status');
		$this->Data->insert_data($keterangan, $id_kode, $nama_dokumen, $id_kategori, $id_status);
		redirect('home/new_script');
	}

	public function hak_akses()
	{
		$this->load->view('header');
		$this->load->view('hak_akses');
		$this->load->view('footer');
	}

	public function user_profile()
	{
		$this->load->view('header');
		$this->load->view('user_profile');
		$this->load->view('footer');
	}

	public function testdb()
	{
		$this->load->view('testdb');
	}
}
