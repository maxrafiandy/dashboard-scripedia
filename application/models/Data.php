<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  function tampil_data()
  {
    $data = $this->db->query("SELECT status label, count(status) / (select count(id) from patching) * 100 jumlah
      FROM patching
      GROUP BY status");
    return $data->result();
  }

  function insert_data($keterangan, $id_kode, $nama_dokumen, $id_kategori, $id_status)
  {
    $data = $this->db->query("INSERT INTO add_script (keterangan,id_kode,nama_dokumen,id_kategori,id_status) 
      VALUES ('$keterangan', '$id_kode', '$nama_dokumen', '$id_kategori', '$id_status')");
    return $data;
  }

  function tampil_datatable()
  {
    $data = $this->db->query("SELECT * FROM add_script");
    return $data->result();
  }
}
